from setuptools import setup

setup(
    name='port-scanner',
    version="main",
    install_requires=[
        'aiohttp[optional]',
        'uvloop',
        'pytest','pytest-aiohttp','pytest-cov',
    ],
    url="https://gitlab.com/RShabanov/port-scanner",
    author="Roman Shabanov",
    author_email="shabanov.roman0@gmail.com"
)