import pytest
from aiohttp import web
from aiohttp.client import ClientResponse


@pytest.fixture
def app() -> web.Application:
    from utils import create_app
    app = create_app()
    yield app


@pytest.fixture
def client(app: web.Application, aiohttp_client, event_loop):
    return event_loop.run_until_complete(aiohttp_client(app))


async def test_index(client):
    response: ClientResponse = await client.get('/')
    assert response.status == 200
    assert response.content_type == "text/html"
    
    text = await response.text()
    assert 'Hello, world' in text
    
    
async def test_scan_ports(client):    
    host = "0.0.0.0"
    begin_port = 10
    end_port = 100
    
    ports_number = end_port - begin_port + 1
    
    response: ClientResponse = await client.get(f"/{host}/{begin_port}/{end_port}")
    assert response.status == 200
    assert response.content_type == "application/json"
        
    data = await response.json()
    assert len(data) == ports_number
    
    
async def test_scan_bad_host(client):
    host = "0.0.0.0badport"
    begin_port = 10
    end_port = 100
    
    ports_number = end_port - begin_port + 1
    
    response: ClientResponse = await client.get(f"/{host}/{begin_port}/{end_port}")
    assert response.status == 400
    assert response.content_type == "application/json"
        
    data = await response.json()
    assert data["error"] == "Host does not exists"
    
    
async def test_scan_bad_ports(client):
    host = "0.0.0.0"
    begin_port = -10
    end_port = 100
    
    ports_number = end_port - begin_port + 1
    
    response: ClientResponse = await client.get(f"/{host}/{begin_port}/{end_port}")
    assert response.status == 400
    assert response.content_type == "application/json"
        
    data = await response.json()
    assert data["error"] == "Invalid ports"
