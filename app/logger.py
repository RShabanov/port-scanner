import logging
from logging.handlers import SysLogHandler


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = SysLogHandler(address="/dev/log")
logger.addHandler(handler)