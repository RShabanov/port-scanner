from socket import AF_INET, SOCK_STREAM, gaierror, gethostbyname, socket
from concurrent.futures import ThreadPoolExecutor, as_completed
from aiohttp.web import StreamResponse, Request

from app.logger import logger

        
class PortScanningController:
    async def scan(self, request: Request) -> None:
        self.response = StreamResponse()
        self.response.content_type = "application/json"
        
        self.encoding = "utf-8"
        
        self.host = request.match_info["host"]
        self.begin_port = request.match_info["begin_port"]
        self.end_port = request.match_info["end_port"]
                
        if not self._host_exists():
            logger.error(f"Bad request: host \"{self.host}\" does not exist")
            await self._error(status=400, msg="Host does not exists", request=request)
        else:
            if self._ports_ok():
                await self._scan_ports(request=request)
            else:
                logger.error(f"Bad request: invalid ports ({self.begin_port}, {self.end_port})")
                await self._error(status=400, msg="Invalid ports", request=request)
        
        await self.response.write_eof()
        
    
    async def _scan_ports(self, request: Request):
        self.response.set_status(200)
        
        if not self.response.prepared:
            await self.response.prepare(request)
        
        ports = range(self.begin_port, self.end_port + 1)
        ports_number = self.end_port - self.begin_port + 1
        
        with ThreadPoolExecutor(max_workers=ports_number) as executor:
            futures = [executor.submit(self._scan_port, port) for port in ports]
            
            await self.response.write('['.encode(self.encoding))
            
            for i, future in enumerate(as_completed(futures)):
                try:
                    port, state = future.result()
                    data = self._ok_json(
                        port=port,
                        state="open" if state else "close"
                    ).encode(self.encoding)
                    
                    await self.response.write(data)
                    
                    if i + 1 != ports_number:
                        await self.response.write(','.encode(self.encoding))
                        
                except Exception as e:
                    logger.error("Port scanner exception: unknown error")
            
            await self.response.write(']'.encode(self.encoding))
    
    
    async def _error(self, status: int, msg: str, request: Request):
        self.response.set_status(status)
        
        if not self.response.prepared:
            await self.response.prepare(request)
            
        await self.response.write(
            self._error_json(msg).encode(encoding=self.encoding)
        )          
            
    
    def _scan_port(self, port: int) -> bool:
        with socket(AF_INET, SOCK_STREAM) as sock:
            sock.settimeout(5)
            return (port, sock.connect_ex((self.host, port)) == 0)
        
    
    def _ports_ok(self) -> bool:
        if self.begin_port.isnumeric() and self.end_port.isnumeric():
            self.begin_port = int(self.begin_port)
            self.end_port = int(self.end_port)
            is_ok = self.begin_port <= self.end_port and self.begin_port >= 0 and self.end_port < 65536
            
            return is_ok
        
        return False
    

    def _host_exists(self) -> bool:
        try:
            gethostbyname(self.host)
            return True
        except gaierror as socker_error:
            return False
        
    
    def _error_json(self, msg: str) -> str:
        return self._to_json({"error": msg})
            
            
    def _ok_json(self, port: int, state: str) -> str:
        return self._to_json({
            "port": port, "state": state
        })
    
            
    def _to_json(self, data: dict) -> str:
        from json import dumps
        
        return dumps(data)
    