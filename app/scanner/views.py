from aiohttp import web
import aiohttp
from aiohttp.web import Request, Response, StreamResponse



def get_index(request: Request) -> Response:
    return Response(
        text=f"""
        <!DOCTYPE html>
        <h1>Scan ports</h1>
        <form method="GET" name="scan-form">
            <div>
                <label>
                    <span>Host</span><br>
                    <input type='text' name='host' placeholder='127.0.0.1'>
                </label>
            </div>
            <div>
                <label>
                    <span>Begin port</span><br>
                    <input type='text' name='beginPort' placeholder='10'>
                </label>
            </div>
            <div>
                <label>
                    <span>End port</span><br>
                    <input type='text' name='endPort' placeholder='100'>
                </label>
            </div>
            
            <button name="scanBtn">Scan</button>
        </form>
        
        <script src="/static/js/scan_port_form.js" lang="javascript"></script>
        """,
        content_type="text/html"
    )