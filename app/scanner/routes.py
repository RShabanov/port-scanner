from app.scanner import views
from aiohttp import web
from aiohttp.web import Response, Request, StreamResponse

from app.scanner.controllers.scanner_controller import PortScanningController


routes = web.RouteTableDef()


@routes.get('/')
async def index(request: Request) -> Response:
    return views.get_index(request)


@routes.get("/{host}/{begin_port:\d+}/{end_port:\d+}")
async def scan_ports(request: Request) -> StreamResponse:
    await PortScanningController().scan(request)


def setup_static():
    routes.static("/static/js/", "static/js")
    

def setup_routes(app: web.Application) -> None:
    setup_static()
    app.add_routes(routes)
    