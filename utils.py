from aiohttp import web


def setup_app(application: web.Application) -> None:
    from app.scanner.routes import setup_routes
    setup_routes(application)
    
    
def create_app() -> web.Application:
    app = web.Application()
    setup_app(app)
    
    return app