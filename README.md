# port-scanner

Test task

## Test coverage:
```
---------- coverage: platform linux, python 3.10.4-final-0 -----------
Name                                            Stmts   Miss  Cover
-------------------------------------------------------------------
app/__init__.py                                     0      0   100%
app/logger.py                                       7      0   100%
app/scanner/__init__.py                             0      0   100%
app/scanner/controllers/scanner_controller.py      68      5    93%
app/scanner/routes.py                              16      0   100%
app/scanner/views.py                                5      0   100%
-------------------------------------------------------------------
TOTAL                                              96      5    95%
```