import argparse
from aiohttp import web
import asyncio
from app.logger import logger
from utils import create_app
    
    
try:
    import uvloop   
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    logger.error("cannot import \'uvloop\' module")
    
    
parser = argparse.ArgumentParser()
parser.add_argument("--host", help="host to listen", default="0.0.0.0")
parser.add_argument("--port", help="port to listen", default=8080)

    
if __name__ == "__main__":
    args = parser.parse_args()
    
    web.run_app(
        app=create_app(),
        host=args.host,
        port=args.port
    )
    