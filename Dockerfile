FROM fedora:35

EXPOSE 3000

WORKDIR /app

COPY . /app/

RUN dnf -y install python3-pip
RUN pip3 install --no-input -r requirements.txt

CMD /bin/bash start.sh