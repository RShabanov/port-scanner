(function() {
    let scanForm = document.forms["scan-form"];

    scanForm.scanBtn.onclick = event => {
        event.preventDefault();

        let host = scanForm.host.value;
        let beginPort = scanForm.beginPort.value;
        let endPort = scanForm.endPort.value;

        if (host.length == 0 || 
            beginPort.length == 0 ||
            endPort.length == 0
        ) {
            alert("Don't leave empty fields");
            return;
        }

        if (isNaN(+beginPort) || isNaN(+endPort)) {
            alert("Port is a number");
            return;
        } 

        window.location.href += `${host}/${beginPort}/${endPort}`;
    }
})()